const products = [
  {
    Pid: 1,
    name: "Lawn Mower",
    price: 5,
    img: require("./src/image/shop.jpg"),
    rent: "63 times",
    loc: "Est. 6.3 KMs",
    qty: 1
  },
  {
    Pid: 2,
    name: "Driller Chordless",
    price: 5,
    img: require("./src/image/driller.jpg"),
    rent: "123 times",
    loc: "Est. 6.1 KMs",
    qty: 1
  },
  {
    Pid: 3,
    name: "Lawn Tractor",
    price: 15,
    img: require("./src/image/tractor.jpg"),
    rent: "12 times",
    loc: "Est. 9.1 KMs",
    qty: 1
  },
  {
    Pid: 4,
    name: "Rake",
    price: 1,
    img: require("./src/image/rake.png"),
    rent: "2 times",
    loc: "Est. 1.1 KMs",
    qty: 1
  }
];
export default products;
