const express = require('express');
const path = require('path');
const multer = require("multer");
const router = express.Router();
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
const productscontroller = require('../controllers/product');
const customercontroller = require('../controllers/customerdetails');
// inserting the product details in database
router.post('/add-product', productscontroller.getaddproducts);
//Getting the product deetails
router.post('/product', productscontroller.postaddproduct);

//Inserting the customer details and fetching the customer ID 
router.post('/customer', customercontroller.postcustomerdetails);

const storage = multer.diskStorage({
    destination: './upload/images',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})
const filefilter = (req, file, cb) => {
    //reject a file
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg') {
        cb(null, true);
    }
    else {
        cb(null, false);
    }
}
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 10000000000
    },
    fileFilter: filefilter,
})
//Geting the list of all product details from database.
router.post("/upload", upload.single('profile'), productscontroller.getaddproducts, (req, res) => {
    console.log(req.file);
    console.log(req.body.name);
    //sending the response of image url in Json format
    res.json({
        success: 1,
        profile_url: `http://localhost:3000/profile/${req.file.filename}`
    })
})

function errHandler(err, req, res, next) {
    if (err instanceof multer.MulterError) {
        res.json({
            success: 0,
            message: err.message
        })
    }
}
router.use(errHandler);
module.exports = router;
