var async = require('async');
var _ = require('lodash');
exports.postcustomerdetails = (req, res, next) => {
    data = [];
    let responsedata = {
        _id: '',
        phonenumbenor: '',
        otp: ''
    }
    let data1 = {
        number: ''
    }
    async.series(
        [
            function (callback) {
                // fetch your data here using mongo with your loop
                //
                req.on('data', (chunk) => {
                    data.push(chunk)

                    data1 = JSON.parse(data)


                    var MongoClient = require('mongodb').MongoClient;
                    var url1 = "mongodb://localhost:27017/";
                    MongoClient.connect(url1, { useUnifiedTopology: true }, function (err, db) {
                        if (err) throw err;
                        var dbo = db.db("mydb");
                        //checking the phone number present in customer details table
                        var val = Math.floor(1000 + Math.random() * 9000);
                        console.log(val);
                        query1 = { "phonenumber": data1.number }

                        dbo.collection("customerdetails").find(query1).toArray(function (err, ress) {
                            if (err) throw err;
                            if (ress.length > 0) {
                                console.log(" phone number  and otp already present in  customer details");

                                var dataconvert = '';
                                dataconvert = JSON.stringify(ress[0]);
                                responsedata = JSON.parse(dataconvert);
                                responsedata.otp = val;
                                callback();
                                //console.log(ress)
                            }
                            else {

                                var myobj = { phonenumber: data1.number, otp: val };
                                dbo.collection("customerdetails").insertOne(myobj, function (err, res) {
                                    if (err) throw err;
                                    console.log(" phone number  and otp inserted into customer details");
                                    query = { "phonenumber": data1.number, "otp": val }
                                    //console.log(query);
                                    dbo.collection("customerdetails").find(query).toArray(function (err, ress) {
                                        if (err) throw err;
                                        //console.log(ress);
                                        var dataconvert = '';
                                        dataconvert = JSON.stringify(ress[0]);
                                        responsedata = JSON.parse(dataconvert);
                                        callback();
                                    });
                                })
                            }
                        })

                    });
                });

                //
                // callback(); // this will trigger next step (call this after you finished iterating array)
            },
            function (callback) {
                // after above code has been executed
                // send response here
                console.log(responsedata);
                res.send(responsedata);
                // callback() // call this to proceed
            }
        ],

        function (err) {
            console.log(err);
            // handle any error in here
        }
    )

}

